package com.greedy.run;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.greedy.controller.MusicController;

public class Application {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		MusicController musicController = new MusicController();
		
		do {
			System.out.println("===== 노래 관리 =====");
			System.out.println("1. 노래 전체 조회");
			System.out.println("2. 노래 추가");
			System.out.println("3. 노래 수정");
			System.out.println("4. 노래 삭제");
			System.out.print("노래 관리 번호를 입력 : ");
			int no = sc.nextInt();
			
			switch(no) {
			case 1: musicController.selectAllMusic(); break;
			case 2: musicController.registMusic(inputMusic()); break;
			case 3: musicController.modifyMusic(inputModifyMusic()); break;
			case 4: musicController.deleteMusic(inputMusicCode()); break;
			default: System.out.println("잘못 된 메뉴를 선택하셨습니다.");
			}
		} while(true);
		
	}
	
   private static Map<String, String> inputMusicCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("메뉴 코드 입력 : ");
		String code = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		
		return parameter;
	}
	
	
	private static Map<String, String> inputMusic() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("노래 이름 : ");
		String name = sc.nextLine();
		System.out.print("가수 : ");
		String artist = sc.nextLine();
		System.out.print("장르 코드 : ");
		String genreCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("name", name);
		parameter.put("artist", artist);
		parameter.put("genreCode", genreCode);
		
		return parameter;
	}
	
	private static Map<String, String> inputModifyMusic() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("수정할 노래 번호 : ");
		String code = sc.nextLine();
		System.out.print("수정할 노래 제목 : ");
		String name = sc.nextLine();
		System.out.print("수정할 가수 이름 : ");
		String artist = sc.nextLine();
		System.out.print("수정할 장르 코드 : ");
		String genreCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		parameter.put("name", name);
		parameter.put("artist", artist);
		parameter.put("genreCode", genreCode);
		
		return parameter;
	}
	
	
	
	
	

}
