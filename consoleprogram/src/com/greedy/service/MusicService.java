package com.greedy.service;

import static com.greedy.common.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.greedy.dao.MusicDAO;
import com.greedy.dto.MusicDTO;

public class MusicService {
	
	private final MusicDAO musicDAO;
	
	public MusicService() {
		musicDAO = new MusicDAO();
	}

	public List<MusicDTO> selectAllMusic() {
		
		SqlSession sqlSession = getSqlSession();
		
		List<MusicDTO> musicList = musicDAO.selectAllMusic(sqlSession);
		
		sqlSession.close();
		
		return musicList;
	}


	public boolean registMusic(MusicDTO menu) {
		
		SqlSession sqlSession = getSqlSession();
		
		int result = musicDAO.insertMusic(sqlSession, menu);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean modifyMusic(MusicDTO menu) {
		
		SqlSession sqlSession = getSqlSession();
		
		int result = musicDAO.updateMusic(sqlSession, menu);
		
		if(result > 0) {
			
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean deleteMusic(int code) {
		
		SqlSession sqlSession = getSqlSession();
		
		int result = musicDAO.deleteMusic(sqlSession, code);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

}







