package com.greedy.dto;

public class MusicDTO {
	
	private int musicCode;
	private String musicName;
	private String artist;
	private int genreCode;
	
	public MusicDTO() {}

	public MusicDTO(int musicCode, String musicName, String artist, int genreCode) {
		super();
		this.musicCode = musicCode;
		this.musicName = musicName;
		this.artist = artist;
		this.genreCode = genreCode;
	}

	public int getMusicCode() {
		return musicCode;
	}

	public void setMusicCode(int musicCode) {
		this.musicCode = musicCode;
	}

	public String getMusicName() {
		return musicName;
	}

	public void setMusicName(String musicName) {
		this.musicName = musicName;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public int getGenreCode() {
		return genreCode;
	}

	public void setGenreCode(int genreCode) {
		this.genreCode = genreCode;
	}

	@Override
	public String toString() {
		return "MusicDTO [musicCode=" + musicCode + ", musicName=" + musicName + ", artist=" + artist + ", genreCode="
				+ genreCode + "]";
	}
	
	


}
