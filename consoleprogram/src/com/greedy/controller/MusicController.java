package com.greedy.controller;

import java.util.List;
import java.util.Map;

import com.greedy.dto.MusicDTO;
import com.greedy.service.MusicService;
import com.greedy.views.PrintResult;

public class MusicController {

	private final MusicService musicService;
	private final PrintResult printResult;
	
	public MusicController() {
		musicService = new MusicService();
		printResult = new PrintResult();
	}
	
	public void selectAllMusic() {
		
		List<MusicDTO> musicList = musicService.selectAllMusic();
		
		if(musicList != null) {
			printResult.printMusicList(musicList);
		} else {
			printResult.printErrorMessage("selectList");
		}
		
	}


	public void registMusic(Map<String, String> parameter) {
		
		MusicDTO menu = new MusicDTO();
		menu.setMusicName(parameter.get("name"));
		menu.setArtist((parameter.get("artist")));
		menu.setGenreCode(Integer.parseInt(parameter.get("genreCode")));
		
		if(musicService.registMusic(menu)) {
			printResult.printSuccessMessage("insert");
		} else {
			printResult.printErrorMessage("insert");
		}
	
	}

	public void modifyMusic(Map<String, String> parameter) {
		
		MusicDTO menu = new MusicDTO();
		menu.setMusicCode(Integer.parseInt(parameter.get("code")));
		menu.setMusicName(parameter.get("name"));
		menu.setArtist((parameter.get("artist")));
		menu.setGenreCode(Integer.parseInt(parameter.get("genreCode")));
		
		if(musicService.modifyMusic(menu)) {
			printResult.printSuccessMessage("update");
		} else {
			printResult.printErrorMessage("update");
		}
		
	}

	public void deleteMusic(Map<String, String> parameter) {
		
		int code = Integer.parseInt(parameter.get("code"));
		
		if(musicService.deleteMusic(code)) {
			printResult.printSuccessMessage("delete");
		} else {
			printResult.printErrorMessage("delete");
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	
}
