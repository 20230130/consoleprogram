package com.greedy.views;

import java.util.List;

import com.greedy.dto.MusicDTO;

public class PrintResult {

	public void printMusicList(List<MusicDTO> musicList) {
		for(MusicDTO menu : musicList) {
			System.out.println(menu);
		}
	}
	
	public void printMusic(MusicDTO menu) {
		System.out.println(menu);
	}

	public void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList" : errorMessage = "노래 목록 조회에 실패하였습니다."; break;
		case "insert" : errorMessage = "노래 등록에 실패하였습니다."; break;
		case "update" : errorMessage = "노래 수정에 실패하였습니다."; break;
		case "delete" : errorMessage = "노래 삭제에 실패하였습니다."; break;
		}
		
		System.out.println(errorMessage);
	}

	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "노래 등록에 성공하였습니다."; break;
		case "update" : successMessage = "노래 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "노래 삭제에 성공하였습니다."; break;
		}
		
		System.out.println(successMessage);
		
	}


}




