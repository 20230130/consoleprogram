package com.greedy.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.greedy.dto.MusicDTO;

public class MusicDAO {

	public List<MusicDTO> selectAllMusic(SqlSession sqlSession) {
		return sqlSession.selectList("MusicMapper.selectAllMusic");
	}

	public int insertMusic(SqlSession sqlSession, MusicDTO menu) {
		return sqlSession.insert("MusicMapper.insertMusic", menu);
	}

	public int updateMusic(SqlSession sqlSession, MusicDTO menu) {
		return sqlSession.update("MusicMapper.updateMusic", menu);
	}

	public int deleteMusic(SqlSession sqlSession, int code) {
		return sqlSession.delete("MusicMapper.deleteMusic", code);
	}

}
